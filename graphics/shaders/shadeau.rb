Debugger.warp(13, 10, 10)
def test
  Shader.register(:water, 'graphics/shaders/water2.frag', 'graphics/shaders/water2.vert', tone_process: true, color_process: true)
  $scene.viewport.shader = Shader.create(:water)
  $scene.viewport.shader.set_texture_uniform('waterTexture', @a ||= Bitmap.new('graphics/particles/astrWater'))
  $scene.viewport.shader.set_texture_uniform('displacementTexture', @b ||= Bitmap.new('graphics/particles/astrSS04'))
  $scene.viewport.shader.set_texture_uniform('stillWaterTexture', @c ||= Bitmap.new('graphics/particles/astrSS06'))
  $scene.viewport.shader.set_float_uniform('screenFactor', [320.0 / 16, 240.0 / 16])
end
test

Hooks.register(Spriteset_Map, :update) do
  @water_wave ||= 0
  @water_wave += 0.05
  @water_wave %= 32
  @water ||= 0
  @water += 0.3
  @water %= 32

  # minus because the y coordinate are reversed in shader
  @viewport1.shader&.set_float_uniform(
    'displayCoordinates',
    [
      ($game_map.display_x / 4 / 32.0) % 1,
      -($game_map.display_y / 4 / 32.0) % 1
    ]
  )
  @viewport1.shader&.set_float_uniform('waterOffset', [@water / 32.0, -@water / 32.0])
  @viewport1.shader&.set_float_uniform('waterWave', [@water_wave / 32.0, @water_wave / 32.0])
end
